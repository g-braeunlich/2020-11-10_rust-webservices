const corsProxy = 'http://localhost:3030/';
const service = 'http://127.0.0.1:';
const logPort = '8100';

function getHandler(endpoint, input, outputCli, port, srvHandler) {
  const useInput = (input !== null);
  return function _handler () {
    fetch(corsProxy + service + port + endpoint + (useInput?input.value:'')).then(
      response => { console.log(response); return response.text(); })
      .then(data => {
        console.log('getHandler', data, outputCli);
	outputCli.textContent = data;})
      .then(srvHandler);
  };
}

function postHandler(endpoint, input, output, port, srvHandler) {
  return function _handler () {
    fetch(corsProxy + service + port + endpoint, {
      method: 'POST',
      headers: {'Content-Type': 'application/json',
		'Origin': 'localhost'},
      body: input.value
    }).then(
      response => { console.log(response); return response.text(); })
      .then(data => { output.textContent = data; })
      .then(srvHandler);
  };
}

const maxLogLength = 12;

function attachLog(dest, log) {
  function _getLogs() {
    getLogs(log, dest);
  }
  _getLogs();
  return _getLogs;
}

function getLogs(log, output) {
  console.log('getLogs');
  fetch(corsProxy + service + logPort + '/' + log).then(
    response => { console.log(response); return response.text(); })
    .then(tail)
    .then(data => { output.textContent = data; });
}

function tail(s) {
  const lines = s.split('\n');
  if(lines.length <= maxLogLength) { return s; }
  return lines.slice(lines.length - 14).join('\n');
}
