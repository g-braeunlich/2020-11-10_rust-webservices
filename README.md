# Rust & webservices

Online version: https://g-braeunlich.gitlab.io/2020-11-10_rust-webservices/

## Use locally

```bash
yarn install
```

Then view `index.html` in your browser.

If you also want the webservices, run this in another terminal:

```bash
git clone https://codeberg.org/g-braeunlich/rest-rocket.git
cd rest-rocket
cargo build --release
cargo install warp-cors
./start.sh
```
